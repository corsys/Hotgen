REFS = "REFS"
REF_SIZE = "REF_SIZE"
def compute_hotset(expstr, thresh, kstype, apfile, mapfile, hotfile,
  verbose=False):

  print ("computing %s %s for %s ... " %
          (th2str(thresh), kstype.lower(), expstr)),

  apinfo = get_ap_info(apfile)
  target = int((sum ( [ v[REF_SIZE] for v in apinfo.values() ] ) \
                    * (thresh/100.0)
              ))
  rpbs   = sorted ( [ (k, (float(v[REFS])/v[REF_SIZE]))
                       for k,v in apinfo.items() if v[REF_SIZE] != 0 ],
                    key=itemgetter(1), reverse=True )

  hotset  = []
  cursize = 0
  for id,rpb in rpbs:
    hotset.append(id)
    cursize += apinfo[id][REF_SIZE]
    if cursize >= target:
      break

  apmap = get_ap_map(mapfile)
  hotf  = try_open_write(hotfile)
  for id in hotset:
    if not apmap.has_key(id):
      print ("error: AP %d not in apmap" % ap)
      raise SystemExit(1)

    rec = apmap[id]
    print >> hotf, ("%-8d %-16d %-16d" % \
                   (id, rec[REF_SIZE], rec[REFS]))
    print >> hotf, rec[AP_CXT]
    print >> hotf, ""
  print "done."


def get_ap_info(apifile):

  apinfo = {}

  try:
    apif = open(apifile)
  except IOError, exc:
    return None

  for line in apif:
    if apidRE.match(line):

      id, ref_size, refs = [int(x) for x in line.split()[:3]]
      apinfo[id] = {}
      apinfo[id][REF_SIZE] = ref_size
      apinfo[id][REFS]     = refs

  return apinfo

def get_ap_map(mapfile):

  apmap = {}

  try:
    mapf = open(mapfile)
  except IOError, exc:
    return None

  for line in mapf:
    if apidRE.match(line):

      id, ref_size, refs = [int(x) for x in line.split()[:4]]

      apcxt = None
      appts = []
      for apline in mapf:
        if apline != '\n':
          appts.append(apline.strip())
        else:
          apcxt = "\n".join(appts)
          apmap[id] = {}
          apmap[id][AP_CXT]   = apcxt
          apmap[id][REF_SIZE] = ref_size
          apmap[id][REFS]     = refs
          break
  return apmap
